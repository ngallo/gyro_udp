//Written by Ahmet Burkay KIRNIK
//TR_CapaFenLisesi
//Measure Angle with a MPU-6050(GY-521)
#include <LiquidCrystal_I2C.h>

// Construct an LCD object and pass it the 
// I2C address, width (in characters) and
// height (in characters). Depending on the
// Actual device, the IC2 address may change.
LiquidCrystal_I2C lcd(0x3F, 16, 2);
#include<Wire.h>
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>

//const int buttonPin = 2; Button to zero

const char* ssid = "ESP8266 Thing 6D87";
const char* password = "testtest";

WiFiUDP Udp;
unsigned int localUdpPort = 4210;  // local port to listen on
char incomingPacket[255];  // buffer for incoming packets

const int MPU_addr=0x68;
int16_t AcX,AcY,AcZ,Tmp,GyX,GyY,GyZ;

float minVal=265.000;
float maxVal=402.000;

double x;
double y;
double z;

#define LED D0
#define INTERRUPT_PIN D2  // use pin 2 on Arduino Uno & most boards

void setup(){
   Wire.begin(D6,D5);
  lcd.begin(16,2);
  lcd.init();

  // Turn on the backlight.
  lcd.backlight();

  // Move the cursor characters to the right and
  // zero characters down (line 1).
  lcd.setCursor(5, 0);
// pinMode(buttonPin, INPUT);

   Serial.begin(115200);
  pinMode(LED, OUTPUT); 
   
  WiFi.begin(ssid, password);

   while (WiFi.status() != WL_CONNECTED)
  {
     lcd.print("TEST");
    digitalWrite(LED, HIGH); 
    delay(250);
    LCD.clear();
    digitalWrite(LED, LOW);
    delay(250); 
  }


  // Print HELLO to the screen, starting at 5,0.
 
 // Serial.println(" connected");

  Udp.begin(localUdpPort);
  Serial.printf("Now listening at IP %s, UDP port %d\n", WiFi.localIP().toString().c_str(), localUdpPort);
String IP = WiFi.localIP().toString().c_str();
   lcd.print(IP);
  Wire.begin(D6,D5);
  Wire.beginTransmission(MPU_addr);
  Wire.write(0x6B);
  Wire.write(0);
  Wire.endTransmission(true);

}
void loop(){

// buttonState = digitalRead(buttonPin);buttonState = digitalRead(buttonPin);
   
  Wire.beginTransmission(MPU_addr);
  Wire.write(0x3B);
  Wire.endTransmission(false);
  Wire.requestFrom(MPU_addr,14,true);
  AcX=Wire.read()<<8|Wire.read();
  AcY=Wire.read()<<8|Wire.read();
  AcZ=Wire.read()<<8|Wire.read();
    float xAng = map(AcX,minVal,maxVal,-90,90);
    float yAng = map(AcY,minVal,maxVal,-90,90);
    float zAng = map(AcZ,minVal,maxVal,-90,90);

       x= RAD_TO_DEG * (atan2(-yAng, -zAng)+ M_PI);
       y= RAD_TO_DEG * (atan2(-xAng, -zAng)+ M_PI);
       z= RAD_TO_DEG * (atan2(-yAng, -xAng)+ M_PI);

   // if (buttonState == HIGH) {
  //  a = x;
  // x = a-x; 
 // } else { }  Problem with this is that I will rewrite everything and it will always be zero 
     
     Serial.print("AngleX= ");
     Serial.println(x );

     Serial.print("AngleY= ");
     Serial.println(y);

     Serial.print("AngleZ= ");
     Serial.println(z);
     Serial.println("-----------------------------------------");
     delay(100);

int packetSize = Udp.parsePacket();
float t = 0;
String s = String(x);
String s1 = String(y);
String s2 = String(z);

char tArray[10];
char eArray[10];
char eArray1[10];
char eArray2[10];

dtostrf(x, 5, 2, eArray);
dtostrf(y, 5, 2, eArray1);
dtostrf(z, 5, 2, eArray2);
dtostrf(t, 5, 0, tArray);

  if (packetSize)
  {
     
    // receive incoming UDP packets
    Serial.printf("Received %d bytes from %s, port %d\n", packetSize, Udp.remoteIP().toString().c_str(), Udp.remotePort());
    int len = Udp.read(incomingPacket, 255);
    if (len > 0)
    {
      incomingPacket[len] = 0;
    }
    Serial.printf("UDP packet contents: %s\n", incomingPacket);

    // send back a reply, to the IP address and port we got the packet from
    Udp.beginPacket(Udp.remoteIP(), Udp.remotePort()+1);

     t = t + 1.0;
     Udp.write("Bob");
     Udp.write(tArray);
     Udp.write(" |");
     Udp.write(eArray);
     Udp.write(" | ");
     Udp.write(eArray1);
     Udp.write(" | ");
     Udp.write(eArray2 );
     Udp.endPacket();
  }
}
